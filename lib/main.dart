import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './providers/categories_provider.dart';
import './providers/transactions_provider.dart';
import './providers/wallet_provider.dart';

import './screens/home_screen.dart';
import './screens/planning_screen.dart';
import './screens/history_screen.dart';
import './screens/categories_screen.dart';
import './screens/add_transaction_screen.dart';
import './screens/edit_category_screen.dart';
import './screens/wallet_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: CategoriesProvider(),
        ),
        ChangeNotifierProvider.value(
          value: TransactionsProvider(),
        ),
        ChangeNotifierProvider.value(
          value: WalletProvider(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primaryColor: Color(0xFF31334D),
          accentColor: Color.fromRGBO(199, 38, 190, 0.7),
        ),
        routes: {
          '/': (ctx) => HomeScreen(),
          PlanningScreen.routeName: (ctx) => PlanningScreen(),
          HistoryScreen.routeName: (ctx) => HistoryScreen(),
          CategoriesScreen.routeName: (ctx) => CategoriesScreen(),
          AddTransactionScreen.routeName: (ctx) => AddTransactionScreen(),
          EditCategoryScreen.routeName: (ctx) => EditCategoryScreen(),
          WalletScreen.routeName: (ctx) => WalletScreen(),
        },
      ),
    );
  }
}
