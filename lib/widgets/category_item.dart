import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../screens/edit_category_screen.dart';
import '../providers/categories_provider.dart';
import '../providers/category.dart';

class CategoryItem extends StatefulWidget {
  final String id;
  final String title;
  final int spendedMoney;
  final int planningMoney;

  CategoryItem({
    @required this.id,
    @required this.title,
    @required this.spendedMoney,
    @required this.planningMoney,
  });

  @override
  _CategoryItemState createState() => _CategoryItemState();
}

class _CategoryItemState extends State<CategoryItem> {
  Future<void> _deleteCategory(id, List<Category> categories) async {
    final updatingCategoryIndex = categories.indexWhere((cat) => cat.id == id);

    if (categories[updatingCategoryIndex].spendedMoney == 0) {
      await Future.wait([
        Provider.of<CategoriesProvider>(context).deleteCategory(id),
      ]);
    } else {
      print('not 0');
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('Удалите транзакции'),
          content: Text('У вас есть транзакции с этой категорией, удалите их'),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        ),
      );
    }

    // if (!(widget.category == 'Бюджет +' || widget.category == 'Бюджет -')) {
    // final updatedCategories = [...categories];
    // updatedCategories[updatingCategoryIndex].spendedMoney -=
    //     int.parse(widget.amount);
    // await Provider.of<CategoriesProvider>(context).updateCategory(
    //     categories[updatingCategoryIndex].id,
    //     updatedCategories[updatingCategoryIndex]);
    // // }
  }

  @override
  Widget build(BuildContext context) {
    final double spendedMoneyPct = widget.spendedMoney / widget.planningMoney;
    final categoriesData = Provider.of<CategoriesProvider>(context);
    final categories = categoriesData.categories;
    return Container(
      height: 60,
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Color.fromRGBO(199, 38, 190, 0.3),
        // border: Border.all(width: 1, color: Colors.white60)
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Stack(
          children: <Widget>[
            FractionallySizedBox(
              widthFactor: spendedMoneyPct,
              child: Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    bottomLeft: Radius.circular(15),
                  ),
                ),
              ),
            ),
            Container(
              height: 60,
              width: double.infinity,
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
              child: Row(
                children: <Widget>[
                  CircleAvatar(
                    child: Icon(Icons.wifi),
                    backgroundColor: Colors.purple[200],
                    foregroundColor: Colors.white,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    widget.title,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '${widget.spendedMoney}/${widget.planningMoney}',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Text(
                        '${(spendedMoneyPct * 100).toStringAsFixed(2)} %',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.edit,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).pushNamed(
                        EditCategoryScreen.routeName,
                        arguments: widget.id,
                      );
                    },
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.delete_forever,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      _deleteCategory(widget.id, categories);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
