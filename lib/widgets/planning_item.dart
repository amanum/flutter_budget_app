import 'package:flutter/material.dart';

class PlanningItem extends StatelessWidget {
  final String id;
  final String title;
  final int spendedMoney;
  final int planningMoney;

  PlanningItem({
    @required this.id,
    @required this.title,
    @required this.spendedMoney,
    @required this.planningMoney,
  });

  @override
  Widget build(BuildContext context) {
    final double spendedMoneyPct = spendedMoney / planningMoney;
    return Container(
      height: 60,
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Color.fromRGBO(199, 38, 190, 0.3),
        // border: Border.all(width: 1, color: Colors.white60)
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Stack(
          children: <Widget>[
            FractionallySizedBox(
              widthFactor: spendedMoneyPct,
              child: Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    bottomLeft: Radius.circular(15),
                  ),
                ),
              ),
            ),
            Container(
              height: 60,
              width: double.infinity,
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
              child: Row(
                children: <Widget>[
                  CircleAvatar(
                    child: Icon(Icons.wifi),
                    backgroundColor: Colors.purple[200],
                    foregroundColor: Colors.white,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    title,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '$spendedMoney/$planningMoney',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Text(
                        '${(spendedMoneyPct * 100).toStringAsFixed(2)} %',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
