import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/wallet_provider.dart';
import '../providers/inc_or_dec.dart';
import '../providers/wallet.dart';
import '../screens/wallet_screen.dart';

class BottomNavBar extends StatefulWidget {
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  var _isInit = true;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<WalletProvider>(context).fetchAndSetWallet().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
      _isInit = false;
    }
    super.didChangeDependencies();
  }

  createAlertDialog(BuildContext context) {
    TextEditingController walletController = TextEditingController();
    Wallet wallet = Provider.of<WalletProvider>(context).myWallet;

    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Бюджет'),
          content: TextField(
            controller: walletController,
            keyboardType: TextInputType.number,
          ),
          actions: <Widget>[
            MaterialButton(
              elevation: 5.0,
              child: Text('Отмена'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            MaterialButton(
              elevation: 5.0,
              child: Icon(Icons.remove_circle_outline),
              onPressed: () async {
                setState(() {
                  _isLoading = true;
                });
                await Provider.of<WalletProvider>(context)
                    .updateWallet(wallet.id,
                        double.parse(walletController.text), IncOrDec.inc)
                    .then((_) {
                  setState(() {
                    _isLoading = false;
                  });
                });
                Navigator.of(context).pop();
              },
            ),
            MaterialButton(
              elevation: 5.0,
              child: Icon(Icons.monetization_on),
              onPressed: () async {
                setState(() {
                  _isLoading = true;
                });
                await Provider.of<WalletProvider>(context)
                    .updateWallet(wallet.id,
                        double.parse(walletController.text), IncOrDec.inc)
                    .then((_) {
                  setState(() {
                    _isLoading = false;
                  });
                });
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final wallet = Provider.of<WalletProvider>(context).myWallet;
    return GestureDetector(
      child: Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 10),
            child: CircleAvatar(
              backgroundColor: Color(0xFF61637D),
              child: RadiantGradientMask(
                child: Icon(
                  Icons.account_balance_wallet,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Баланс',
                style: TextStyle(
                  color: Colors.grey[300],
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              _isLoading
                  ? CircularProgressIndicator()
                  : Text(
                      wallet == null ? '0' : '${wallet.amount.toString()} тг.',
                      style: TextStyle(color: Colors.green[300])),
            ],
          )
        ],
      ),
      onTap: () {
        // createAlertDialog(context);
        Navigator.of(context).pushNamed(WalletScreen.routeName);
      },
    );
  }
}

class RadiantGradientMask extends StatelessWidget {
  RadiantGradientMask({this.child});
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (bounds) => RadialGradient(
        center: Alignment.topCenter,
        radius: 0.5,
        colors: [
          Color.fromRGBO(121, 74, 255, 1),
          Color.fromRGBO(81, 155, 255, 1)
        ],
        tileMode: TileMode.mirror,
      ).createShader(bounds),
      child: child,
    );
  }
}
