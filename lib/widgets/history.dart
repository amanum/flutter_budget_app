import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './transaction_item.dart';
import '../providers/transactions_provider.dart';

class History extends StatefulWidget {
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  var _isInit = true;
  var _isLoading = false;

  @override
  void initState() {
    // Future.delayed(Duration.zero).then((_) {
    //   Provider.of<TransactionsProvider>(context).fetchAndSetTransactions();
    // });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<TransactionsProvider>(context)
          .fetchAndSetTransactions()
          .then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  Future<void> _refreshTransactions() async {
    await Provider.of<TransactionsProvider>(context).fetchAndSetTransactions();
  }

  @override
  Widget build(BuildContext context) {
    final transactionsData = Provider.of<TransactionsProvider>(context);
    final transactions = transactionsData.transactions;
    return RefreshIndicator(
      onRefresh: _refreshTransactions,
      child: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : (transactions.length > 0
              ? ListView(
                  children: transactions
                      .map((transaction) => TransactionItem(
                            id: transaction.id,
                            title: transaction.title,
                            category: transaction.category,
                            amount: transaction.amount,
                          ))
                      .toList(),
                )
              : Center(
                  child: Text('У вас еще нет транзакций'),
                )),
    );
  }
}
