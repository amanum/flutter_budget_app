import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './planning_item.dart';
import '../providers/categories_provider.dart';
import '../providers/transactions_provider.dart';

class Planning extends StatefulWidget {
  @override
  _PlanningState createState() => _PlanningState();
}

class _PlanningState extends State<Planning> {
  var _isInit = true;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<CategoriesProvider>(context)
          .fetchAndSetCategories()
          .then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  Future<void> _refreshCategories() async {
    await Provider.of<CategoriesProvider>(context).fetchAndSetCategories();
  }

  @override
  Widget build(BuildContext context) {
    final categoriesData = Provider.of<CategoriesProvider>(context);
    final categories = categoriesData.categories;
    return RefreshIndicator(
      onRefresh: _refreshCategories,
      child: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView(
              children: categories
                  .map((category) => PlanningItem(
                        id: category.id,
                        title: category.title,
                        spendedMoney: category.spendedMoney,
                        planningMoney: category.planningMoney,
                      ))
                  .toList(),
            ),
    );
  }
}
