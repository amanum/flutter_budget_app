import 'package:flutter/material.dart';

import '../screens/planning_screen.dart';
import '../screens/categories_screen.dart';
import '../screens/history_screen.dart';
import '../screens/wallet_screen.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Theme.of(context).primaryColor,
        child: Column(
          children: <Widget>[
            AppBar(
              title: Text('Меню'),
              automaticallyImplyLeading: false,
              elevation: 0.0,
            ),
            ListTile(
              leading: CircleAvatar(
                child: Icon(Icons.home),
                backgroundColor: Color(0xFF61637D),
                foregroundColor: Colors.white,
              ),
              title: Text(
                'Главная',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
                Navigator.of(context).pushNamed('/');
              },
            ),
            ListTile(
              leading: CircleAvatar(
                child: Icon(Icons.event_note),
                backgroundColor: Color(0xFF61637D),
                foregroundColor: Colors.white,
              ),
              title: Text(
                'Планирование',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
                Navigator.of(context).pushNamed(PlanningScreen.routeName);
              },
            ),
            ListTile(
              leading: CircleAvatar(
                child: Icon(Icons.history),
                backgroundColor: Color(0xFF61637D),
                foregroundColor: Colors.white,
              ),
              title: Text(
                'История',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
                Navigator.of(context).pushNamed(HistoryScreen.routeName);
              },
            ),
            ListTile(
              leading: CircleAvatar(
                child: Icon(Icons.category),
                backgroundColor: Color(0xFF61637D),
                foregroundColor: Colors.white,
              ),
              title: Text(
                'Категории',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
                Navigator.of(context).pushNamed(CategoriesScreen.routeName);
              },
            ),
            ListTile(
              leading: CircleAvatar(
                child: Icon(Icons.account_balance_wallet),
                backgroundColor: Color(0xFF61637D),
                foregroundColor: Colors.white,
              ),
              title: Text(
                'Бюджет',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
                Navigator.of(context).pushNamed(WalletScreen.routeName);
              },
            ),
          ],
        ),
      ),
    );
  }
}
