import 'package:flutter/material.dart';

import '../providers/inc_or_dec.dart';

class TransactionItem extends StatelessWidget {
  final String id;
  final String title;
  final String category;
  final String amount;
  final IncOrDec incOrDec;

  TransactionItem({
    @required this.id,
    @required this.title,
    @required this.category,
    @required this.amount,
    @required this.incOrDec,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Color.fromRGBO(121, 74, 255, 1),
                Color.fromRGBO(81, 155, 255, 1),
              ],
            ),
            borderRadius: BorderRadius.circular(15)),
        margin: EdgeInsets.all(5),
        child: ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 5),
          leading: CircleAvatar(
            child: Icon(
              Icons.face,
            ),
            backgroundColor: Colors.purple[200],
            foregroundColor: Colors.white,
          ),
          title: Text(
            title,
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          subtitle: Text(
            category,
            style: TextStyle(color: Colors.white60),
          ),
          trailing: category == 'Бюджет +'
              ? Text(
                  '+ $amount тг.',
                  style: TextStyle(
                    color: Colors.green[200],
                  ),
                )
              : Text(
                  '- $amount тг.',
                  style: TextStyle(
                    color: Colors.red[200],
                  ),
                ),
        ),
      ),
    );
  }
}
