import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/transactions_provider.dart';
import '../providers/categories_provider.dart';
import '../providers/category.dart';

class HistoryTransactionItem extends StatefulWidget {
  final String id;
  final String title;
  final String category;
  final String amount;

  HistoryTransactionItem({
    @required this.id,
    @required this.title,
    @required this.category,
    @required this.amount,
  });

  @override
  _HistoryTransactionItemState createState() => _HistoryTransactionItemState();
}

class _HistoryTransactionItemState extends State<HistoryTransactionItem> {
  Future<void> _deleteTransaction(id, List<Category> categories) async {
    final updatingCategoryIndex =
        categories.indexWhere((cat) => cat.title == widget.category);

    if (!(widget.category == 'Бюджет +' || widget.category == 'Бюджет -')) {
      final updatedCategories = [...categories];
      updatedCategories[updatingCategoryIndex].spendedMoney -=
          int.parse(widget.amount);
      await Provider.of<CategoriesProvider>(context).updateCategory(
          categories[updatingCategoryIndex].id,
          updatedCategories[updatingCategoryIndex]);
    }

    await Future.wait([
      Provider.of<TransactionsProvider>(context).deleteTransaction(widget.id),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    final categoriesData = Provider.of<CategoriesProvider>(context);
    final categories = categoriesData.categories;
    return Dismissible(
      key: ValueKey(widget.id),
      background: Container(
        color: Theme.of(context).errorColor,
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40,
        ),
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 20),
        margin: EdgeInsets.all(5),
      ),
      direction: DismissDirection.endToStart,
      onDismissed: (direction) {
        _deleteTransaction(widget.id, categories);
      },
      child: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Color.fromRGBO(121, 74, 255, 1),
                Color.fromRGBO(81, 155, 255, 1),
              ],
            ),
            borderRadius: BorderRadius.circular(15),
          ),
          margin: EdgeInsets.all(5),
          child: ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 5),
            leading: CircleAvatar(
              child: Icon(
                Icons.face,
              ),
              backgroundColor: Colors.purple[200],
              foregroundColor: Colors.white,
            ),
            title: Text(
              widget.title,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            subtitle: Text(
              widget.category,
              style: TextStyle(color: Colors.white60),
            ),
            trailing: Text(
              '${widget.amount} тг.',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
