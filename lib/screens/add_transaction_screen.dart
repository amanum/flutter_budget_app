import 'package:flutter/material.dart';
import 'package:flutter_budget/providers/wallet.dart';
import 'package:provider/provider.dart';

import '../providers/transactions_provider.dart';
import '../providers/transaction.dart';
import '../providers/categories_provider.dart';
import '../providers/category.dart';
import '../providers/wallet_provider.dart';
import '../providers/inc_or_dec.dart';

class AddTransactionScreen extends StatefulWidget {
  static const routeName = '/add-transaction-screen';

  @override
  _AddTransactionScreenState createState() => _AddTransactionScreenState();
}

class _AddTransactionScreenState extends State<AddTransactionScreen> {
  final _form = GlobalKey<FormState>();
  var _isInit = true;
  var _isLoading = false;
  // String _currentCategory = 'Продукты';

  var newTransaction = Transaction(
    title: '',
    amount: '',
    category: 'Одежда',
    id: DateTime.now().toString(),
  );

  Future<void> _saveForm(List<Category> categories) async {
    Wallet wallet = Provider.of<WalletProvider>(context).myWallet;
    _form.currentState.save();
    setState(() {
      _isLoading = true;
    });

    final updatingCategoryIndex =
        categories.indexWhere((cat) => cat.title == newTransaction.category);

    final updatedCategories = [...categories];
    updatedCategories[updatingCategoryIndex].spendedMoney +=
        int.parse(newTransaction.amount);

    try {
      await Future.wait([
        Provider.of<TransactionsProvider>(context)
            .addTransaction(newTransaction),
        Provider.of<CategoriesProvider>(context).updateCategory(
            categories[updatingCategoryIndex].id,
            updatedCategories[updatingCategoryIndex]),
        Provider.of<WalletProvider>(context).updateWallet(
            wallet.id, double.parse(newTransaction.amount), IncOrDec.dec)
      ]);
    } catch (error) {
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('Ошибка крч'),
          content: Text('херня с запросом, глянь его'),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        ),
      );
    } finally {
      setState(() {
        _isLoading = false;
      });
      Navigator.of(context).pop();
      print(newTransaction);
    }
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<CategoriesProvider>(context)
          .fetchAndSetCategories()
          .then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final categoriesData = Provider.of<CategoriesProvider>(context);
    final categories = categoriesData.categories;
    return Scaffold(
      appBar: AppBar(
        title: Text('Добавить транзакцию'),
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              padding: EdgeInsets.all(5),
              child: Form(
                key: _form,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      decoration: InputDecoration(labelText: 'Товар'),
                      textInputAction: TextInputAction.next,
                      onSaved: (value) {
                        setState(() {
                          newTransaction = Transaction(
                            id: newTransaction.id,
                            category: newTransaction.category,
                            amount: newTransaction.amount,
                            title: value,
                          );
                        });
                      },
                    ),
                    TextFormField(
                      decoration: InputDecoration(labelText: 'Сумма'),
                      keyboardType: TextInputType.number,
                      onSaved: (value) {
                        setState(() {
                          newTransaction = Transaction(
                            id: newTransaction.id,
                            category: newTransaction.category,
                            amount: value,
                            title: newTransaction.title,
                          );
                        });
                      },
                    ),
                    DropdownButton<String>(
                      value: newTransaction.category,
                      onChanged: (String val) {
                        setState(() {
                          newTransaction = Transaction(
                            id: newTransaction.id,
                            category: val,
                            amount: newTransaction.amount,
                            title: newTransaction.title,
                          );
                        });
                      },
                      hint: Text('Выберите категорию'),
                      items:
                          categories.map<DropdownMenuItem<String>>((category) {
                        return DropdownMenuItem<String>(
                          value: category.title,
                          child: Text(category.title),
                        );
                      }).toList(),
                    ),
                    Spacer(),
                    Container(
                      width: double.infinity,
                      child: RaisedButton(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          'Добавить',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        color: Theme.of(context).accentColor,
                        onPressed: () {
                          _saveForm(categories);
                        },
                      ),
                    )
                  ],
                ),
              ),
            ),
    );
  }
}
