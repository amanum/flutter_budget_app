import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_picker/flutter_picker.dart';

import '../providers/category.dart';
import '../providers/categories_provider.dart';

class EditCategoryScreen extends StatefulWidget {
  static const routeName = 'edit-category-screen';
  @override
  _EditCategoryScreenState createState() => _EditCategoryScreenState();
}

class _EditCategoryScreenState extends State<EditCategoryScreen> {
  final _form = GlobalKey<FormState>();

  var _editedCategory = Category(
    title: '',
    icon: '',
    planningMoney: 0,
    spendedMoney: 0,
    id: null,
  );
  var _initValues = {
    'title': '',
    'icon': '',
    'planningMoney': 0,
    'spendedMoney': 0,
  };
  var _isInit = true;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      final categoryId = ModalRoute.of(context).settings.arguments as String;
      if (categoryId != null) {
        _editedCategory =
            Provider.of<CategoriesProvider>(context, listen: false)
                .findById(categoryId);
        _initValues = {
          'title': _editedCategory.title,
          'icon': '',
          'planningMoney': _editedCategory.planningMoney.toString(),
          'spendedMoney': _editedCategory.spendedMoney.toString(),
        };
      }
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  Future<void> _saveForm() async {
    final isValid = _form.currentState.validate();
    if(!isValid) {
      return;
    }
    _form.currentState.save();
    setState(() {
      _isLoading = true;
    });
    if (_editedCategory.id != null) {
      try {
        await Provider.of<CategoriesProvider>(context, listen: false)
            .updateCategory(_editedCategory.id, _editedCategory);
      } catch (error) {
        await showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Ошибка крч'),
            content: Text('херня с запросом, глянь его'),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          ),
        );
      } finally {
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pop();
        print(_editedCategory);
      }
    } else {
      try {
        await Provider.of<CategoriesProvider>(context, listen: false)
            .addCategory(_editedCategory);
      } catch (error) {
        await showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Ошибка крч'),
            content: Text('херня с запросом, глянь его'),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          ),
        );
      } finally {
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pop();
        print(_editedCategory);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_editedCategory.id != null
            ? 'Редактировать категорию'
            : 'Добавить категорию'),
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              padding: EdgeInsets.all(5),
              child: Form(
                key: _form,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      initialValue: _initValues['title'],
                      decoration: InputDecoration(labelText: 'Категория'),
                      textInputAction: TextInputAction.next,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please provide a title.';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        setState(() {
                          _editedCategory = Category(
                            id: _editedCategory.id,
                            title: value,
                            icon: _editedCategory.icon,
                            planningMoney: _editedCategory.planningMoney,
                            spendedMoney: _editedCategory.spendedMoney,
                          );
                        });
                      },
                    ),
                    TextFormField(
                      initialValue: _initValues['planningMoney'].toString(),
                      decoration:
                          InputDecoration(labelText: 'Запланированная сумма'),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please provide a planning amount.';
                        } else if (double.parse(value) <= 0) {
                          return 'Planning amount should be more than 0';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        setState(() {
                          _editedCategory = Category(
                            id: _editedCategory.id,
                            title: _editedCategory.title,
                            icon: _editedCategory.icon,
                            planningMoney: int.parse(value),
                            spendedMoney: _editedCategory.spendedMoney,
                          );
                        });
                      },
                    ),
                    Spacer(),
                    Container(
                      width: double.infinity,
                      child: RaisedButton(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          _editedCategory.id != null ? 'Сохранить' : 'Добавить',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        color: Theme.of(context).accentColor,
                        onPressed: _saveForm,
                      ),
                    )
                  ],
                ),
              ),
            ),
    );
  }
}
