import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/wallet_provider.dart';
import '../providers/wallet.dart';
import '../providers/inc_or_dec.dart';
import '../providers/transaction.dart';
import '../providers/transactions_provider.dart';

class WalletScreen extends StatelessWidget {
  static const routeName = '/wallet-screen';
  @override
  Widget build(BuildContext context) {
    TextEditingController walletController = TextEditingController();
    Wallet wallet = Provider.of<WalletProvider>(context).myWallet;

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        title: Text('Бюджет'),
        elevation: 0.0,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: 10),
                  child: CircleAvatar(
                    backgroundColor: Color(0xFF61637D),
                    child: RadiantGradientMask(
                      child: Icon(
                        Icons.account_balance_wallet,
                        color: Colors.white,
                        size: 30,
                      ),
                    ),
                  ),
                ),
                Text(
                  wallet == null ? '0' : '${wallet.amount.toString()} тг.',
                  style: TextStyle(
                    color: Colors.green[300],
                    fontWeight: FontWeight.bold,
                    fontSize: 35,
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        color: Color(0xFF61637D),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      padding: EdgeInsets.all(10),
                      margin: EdgeInsets.only(right: 10),
                      child: TextField(
                        controller: walletController,
                        keyboardType: TextInputType.number,
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  Container(
                    width: 45,
                    height: 45,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: Colors.red[300],
                    ),
                    margin: EdgeInsets.only(right: 5),
                    child: IconButton(
                      icon: Icon(
                        Icons.remove_circle_outline,
                        size: 30,
                      ),
                      onPressed: () async {
                        final transaction = Transaction(
                          amount: walletController.text,
                          category: 'Бюджет -',
                          title: 'Зарплата',
                        );
                        await Provider.of<WalletProvider>(context)
                            .updateWallet(
                                wallet.id,
                                double.parse(walletController.text),
                                IncOrDec.dec)
                            .then((_) {});
                        await Provider.of<TransactionsProvider>(context)
                            .addTransaction(transaction);
                      },
                    ),
                  ),
                  Container(
                    width: 45,
                    height: 45,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: Colors.green[300],
                    ),
                    padding: EdgeInsets.all(0),
                    child: IconButton(
                      icon: Icon(
                        Icons.add_circle_outline,
                        size: 30,
                      ),
                      onPressed: () async {
                        final transaction = Transaction(
                          amount: walletController.text,
                          category: 'Бюджет +',
                          title: 'Зарплата',
                        );
                        await Provider.of<WalletProvider>(context)
                            .updateWallet(
                                wallet.id,
                                double.parse(walletController.text),
                                IncOrDec.inc)
                            .then((_) {});
                        await Provider.of<TransactionsProvider>(context)
                            .addTransaction(transaction);
                      },
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class RadiantGradientMask extends StatelessWidget {
  RadiantGradientMask({this.child});
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (bounds) => RadialGradient(
        center: Alignment.topCenter,
        radius: 0.5,
        colors: [
          Color.fromRGBO(121, 74, 255, 1),
          Color.fromRGBO(81, 155, 255, 1)
        ],
        tileMode: TileMode.mirror,
      ).createShader(bounds),
      child: child,
    );
  }
}
