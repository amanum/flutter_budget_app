import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';
import '../widgets/planning.dart';
import '../widgets/history.dart';
import './add_transaction_screen.dart';
import '../widgets/bottom_navbar.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    final appBar = AppBar(
      title: Text('Budget App'),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.add),
          onPressed: () {
            Navigator.of(context).pushNamed(AddTransactionScreen.routeName);
          },
        )
      ],
      elevation: 0.0,
    );

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: appBar,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: (mediaQuery.size.height -
                      appBar.preferredSize.height -
                      mediaQuery.padding.top -
                      10) *
                  0.45,
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xFF61637D),
                  borderRadius: BorderRadius.circular(15),
                ),
                padding: EdgeInsets.all(5),
                child: Planning(),
              ),
            ),
            SizedBox(height: 10),
            Container(
              height: (mediaQuery.size.height -
                      appBar.preferredSize.height -
                      mediaQuery.padding.top -
                      10) *
                  0.45,
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xFF61637D),
                  borderRadius: BorderRadius.circular(15),
                ),
                padding: EdgeInsets.all(5),
                child: History(),
              ),
            ),
          ],
        ),
      ),
      drawer: MainDrawer(),
      bottomNavigationBar: Container(
        height: (mediaQuery.size.height -
                appBar.preferredSize.height -
                mediaQuery.padding.top -
                10) *
            0.1,
        // decoration: BoxDecoration(
        //   color: Color(0xFF61637D),
        // ),
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: BottomNavBar(),
      ),
    );
  }
}
