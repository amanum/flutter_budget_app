import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/transactions_provider.dart';
import '../widgets/history_transaction_item.dart';

class HistoryScreen extends StatefulWidget {
  static const routeName = '/history-screen';

  @override
  _HistoryScreenState createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  var _isInit = true;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<TransactionsProvider>(context)
          .fetchAndSetTransactions()
          .then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  Future<void> _refreshTransactions() async {
    await Provider.of<TransactionsProvider>(context).fetchAndSetTransactions();
  }

  @override
  Widget build(BuildContext context) {
    final transactionsData = Provider.of<TransactionsProvider>(context);
    final transactions = transactionsData.transactions;
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        title: Text('История'),
        actions: <Widget>[
          PopupMenuButton(
            icon: Icon(Icons.more_vert),
            itemBuilder: (_) => [
              PopupMenuItem(child: Text('Продукты'), value: 0),
              PopupMenuItem(child: Text('Рестораны'), value: 1),
              PopupMenuItem(child: Text('Дом'), value: 2),
              PopupMenuItem(child: Text('Счета'), value: 3),
              PopupMenuItem(child: Text('Одежда'), value: 4),
            ],
            onSelected: (int selectedVal) {},
          )
        ],
        elevation: 0.0,
      ),
      body: RefreshIndicator(
        onRefresh: _refreshTransactions,
        child: _isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : (transactions.length > 0
                ? ListView(
                    children: transactions
                        .map((transaction) => HistoryTransactionItem(
                              id: transaction.id,
                              title: transaction.title,
                              category: transaction.category,
                              amount: transaction.amount,
                            ))
                        .toList(),
                  )
                : Center(
                    child: Text('У вас еще нет транзакций'),
                  )),
      ),
    );
  }
}
