import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/categories_provider.dart';
import '../widgets/planning_item.dart';

class PlanningScreen extends StatelessWidget {
  static const routeName = '/planning-screen';
  @override
  Widget build(BuildContext context) {
    final categoriesData = Provider.of<CategoriesProvider>(context);
    final categories = categoriesData.categories;
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        title: Text('Планирование'),
        elevation: 0.0,
      ),
      body: ListView(
        children: categories
            .map((category) => PlanningItem(
                  id: category.id,
                  title: category.title,
                  spendedMoney: category.spendedMoney,
                  planningMoney: category.planningMoney,
                ))
            .toList(),
      ),
    );
  }
}
