import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/categories_provider.dart';
import '../widgets/category_item.dart';
import '../screens/edit_category_screen.dart';

class CategoriesScreen extends StatelessWidget {
  static const routeName = '/categories-screen';
  @override
  Widget build(BuildContext context) {
    final categoriesData = Provider.of<CategoriesProvider>(context);
    final categories = categoriesData.categories;
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        title: Text('Категории'),
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.of(context).pushNamed(EditCategoryScreen.routeName);
            },
          )
        ],
      ),
      body: ListView(
        children: categories
            .map((category) => CategoryItem(
                  id: category.id,
                  title: category.title,
                  spendedMoney: category.spendedMoney,
                  planningMoney: category.planningMoney,
                ))
            .toList(),
      ),
    );
  }
}
