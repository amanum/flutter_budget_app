import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import './category.dart';

class CategoriesProvider with ChangeNotifier {
  List<Category> _categories = [];
  List<String> _categoriesTitles = [];

  List<Category> get categories {
    return [..._categories];
  }

  List<String> get categoriesTitles {
    return [..._categoriesTitles];
  }

  Future<void> fetchAndSetCategories() async {
    const url = 'https://flutterbudgetapp.firebaseio.com/categories.json';
    try {
      final res = await http.get(url);
      final data = json.decode(res.body) as Map<String, dynamic>;
      final List<Category> loadedCategories = [];
      final List<String> loadedCategoriesTitles = [];
      data.forEach((categoryId, categoryData) {
        loadedCategories.add(
          Category(
            id: categoryId,
            title: categoryData['title'],
            icon: categoryData['icon'],
            planningMoney: categoryData['planningMoney'],
            spendedMoney: categoryData['spendedMoney'],
          ),
        );
        loadedCategoriesTitles.add(categoryData['title']);
      });
      _categories = loadedCategories;
      _categoriesTitles = loadedCategoriesTitles;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<void> addCategory(Category category) async {
    const url = 'https://flutterbudgetapp.firebaseio.com/categories.json';
    try {
      final res = await http.post(
        url,
        body: json.encode({
          'title': category.title,
          'icon': category.icon,
          'spendedMoney': category.spendedMoney,
          'planningMoney': category.planningMoney,
        }),
      );
      final newCategory = Category(
        id: json.decode(res.body)['name'],
        title: category.title,
        icon: category.icon,
        planningMoney: category.planningMoney,
        spendedMoney: category.spendedMoney,
      );
      _categories.add(newCategory);
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  Future<void> updateCategory(String id, Category updatedCategory) async {
    final categoryIndex = _categories.indexWhere((cat) => cat.id == id);
    final url = 'https://flutterbudgetapp.firebaseio.com/categories/$id.json';
    await http.patch(
      url,
      body: json.encode({
        'title': updatedCategory.title,
        'icon': updatedCategory.icon,
        'spendedMoney': updatedCategory.spendedMoney,
        'planningMoney': updatedCategory.planningMoney,
      }),
    );
    _categories[categoryIndex] = updatedCategory;
    notifyListeners();
  }

  Category findById(String id) {
    return _categories.firstWhere((cat) => cat.id == id);
  }

  Future<void> deleteCategory(String id) async {
    final url = 'https://flutterbudgetapp.firebaseio.com/categories/$id.json';
    final existingCategoryIndex =
        _categories.indexWhere((category) => category.id == id);
    var existingTransaction = _categories[existingCategoryIndex];
    _categories.removeAt(existingCategoryIndex);
    notifyListeners();
    final response = await http.delete(url);
    if (response.statusCode >= 400) {
      _categories.insert(existingCategoryIndex, existingTransaction);
      notifyListeners();
    }
    existingTransaction = null;
  }
}
