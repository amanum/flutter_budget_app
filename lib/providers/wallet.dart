import 'package:flutter/foundation.dart';

class Wallet with ChangeNotifier {
  final String id;
  final double amount;

  Wallet({
    @required this.id,
    @required this.amount,
  });
}
