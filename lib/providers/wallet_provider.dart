import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import './inc_or_dec.dart';
import './wallet.dart';

class WalletProvider with ChangeNotifier {
  double _walletAmount = 0;
  Wallet myWallet;

  Future<void> fetchAndSetWallet() async {
    const url = 'https://flutterbudgetapp.firebaseio.com/wallet.json';
    try {
      final res = await http.get(url);
      final data = json.decode(res.body) as Map<String, dynamic>;
      if (data != null) {
        // _walletAmount = data;
        print(data);
        data.forEach((walletId, walletData) {
          myWallet = Wallet(
            id: walletId,
            amount: walletData['amount'],
          );
          _walletAmount = walletData['amount'];
        });
      } else {
        await http.post(
          url,
          body: json.encode({'amount': 0.00}),
        );
      }
      print(_walletAmount);
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<void> updateWallet(String id, double amount, IncOrDec incOrDec) async {
    final url = 'https://flutterbudgetapp.firebaseio.com/wallet/$id.json';
    double newAmount;

    if (incOrDec == IncOrDec.inc) {
      newAmount = _walletAmount + amount;
    } else {
      newAmount = _walletAmount - amount;
    }

    try {
      await http.patch(
        url,
        body: json.encode({
          'amount': newAmount,
        }),
      );
      notifyListeners();
      fetchAndSetWallet();
    } catch (error) {
      throw error;
    }
  }
}
