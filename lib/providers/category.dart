import 'package:flutter/foundation.dart';

class Category with ChangeNotifier {
  final String id;
  final String title;
  final String icon;
  int spendedMoney;
  int planningMoney;

  Category({
    @required this.id,
    @required this.title,
    @required this.icon,
    @required this.spendedMoney,
    @required this.planningMoney,
  });
}