import 'package:flutter/material.dart';

class Transaction with ChangeNotifier {
  final String id;
  final String title;
  final String category;
  final String amount;

  Transaction({
    @required this.id,
    @required this.title,
    @required this.category,
    @required this.amount,
  });
}
