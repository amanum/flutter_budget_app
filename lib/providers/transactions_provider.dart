import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

import './transaction.dart';

class TransactionsProvider with ChangeNotifier {
  List<Transaction> _transactions = [];

  List<Transaction> get transactions {
    return [..._transactions.reversed];
  }

  Future<void> fetchAndSetTransactions() async {
    const url = 'https://flutterbudgetapp.firebaseio.com/transactions.json';
    try {
      final res = await http.get(url);
      final data = json.decode(res.body) as Map<String, dynamic>;
      final List<Transaction> loadedTransactions = [];
      if (data != null) {
        data.forEach((transactionId, transactionData) {
          loadedTransactions.add(
            Transaction(
              id: transactionId,
              amount: transactionData['amount'],
              category: transactionData['category'],
              title: transactionData['title'],
            ),
          );
        });
      }
      _transactions = loadedTransactions;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<void> addTransaction(Transaction transaction) async {
    const url = 'https://flutterbudgetapp.firebaseio.com/transactions.json';
    try {
      final res = await http.post(
        url,
        body: json.encode({
          'title': transaction.title,
          'category': transaction.category,
          'amount': transaction.amount,
        }),
      );
      final newTransaction = Transaction(
        id: json.decode(res.body)['name'],
        amount: transaction.amount,
        category: transaction.category,
        title: transaction.title,
      );
      _transactions.add(newTransaction);
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  Future<void> deleteTransaction(String id) async {
    final url = 'https://flutterbudgetapp.firebaseio.com/transactions/$id.json';
    final existingTransactionIndex =
        _transactions.indexWhere((transaction) => transaction.id == id);
    var existingTransaction = _transactions[existingTransactionIndex];
    _transactions.removeAt(existingTransactionIndex);
    notifyListeners();
    final response = await http.delete(url);
    if (response.statusCode >= 400) {
      _transactions.insert(existingTransactionIndex, existingTransaction);
      notifyListeners();
    }
    existingTransaction = null;
  }
}
